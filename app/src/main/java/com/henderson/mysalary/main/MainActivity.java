package com.henderson.mysalary.main;

import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.henderson.mysalary.BaseActivity;
import com.henderson.mysalary.R;
import com.henderson.mysalary.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity{

	private MainViewModel viewModel;
	private ActivityMainBinding binding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		initToolBar();
		initAd();
//		setNotiAnimation();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void initView(){
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
		binding.setViewmodel(viewModel);
		binding.setLifecycleOwner(this);
	}

	private void initToolBar(){
		setSupportActionBar(binding.mainToolbar);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
	}

	private void initAd(){
		MobileAds.initialize(this, new OnInitializationCompleteListener() {
			@Override
			public void onInitializationComplete(InitializationStatus initializationStatus) {
			}
		});
		AdRequest adRequest = new AdRequest.Builder().build();
		binding.mainAd.loadAd(adRequest);
	}

//	private ArrayList<String> noticeList = new ArrayList<>();
//	private int noticeCount = 0;
//	private int noticeTerm = 1500;
//	private void setNotiAnimation(){
//		noticeList.add(getResources().getString(R.string.main_notice_1));
////		noticeList.add(getResources().getString(R.string.main_notice_2));
//
//		Animation fromRight = AnimationUtils.loadAnimation(this, R.anim.move_from_right);
//		Animation toLeft = AnimationUtils.loadAnimation(this, R.anim.move_to_left);
//
//		fromRight.setAnimationListener(new Animation.AnimationListener() {
//			@Override
//			public void onAnimationStart(Animation animation) {
//				binding.mainNotice.setText(noticeList.get(noticeCount));
//			}
//
//			@Override
//			public void onAnimationEnd(Animation animation) {
//				final Handler handler = new Handler();
//				handler.postDelayed(() -> {
//					binding.mainNotice.startAnimation(toLeft);
//				}, noticeTerm);
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {}
//		});
//
//		toLeft.setAnimationListener(new Animation.AnimationListener() {
//			@Override
//			public void onAnimationStart(Animation animation) {}
//
//			@Override
//			public void onAnimationEnd(Animation animation) {
//				if(noticeCount < noticeList.size()-1) noticeCount++;
//				else noticeCount = 0;
//				binding.mainNotice.startAnimation(fromRight);
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {}
//		});
//
//		binding.mainNotice.startAnimation(fromRight);
//	}
}
