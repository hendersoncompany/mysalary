package com.henderson.mysalary.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.henderson.mysalary.R;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.model.Job;
import com.henderson.mysalary.model.Month;
import com.henderson.mysalary.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarAdapter extends BaseAdapter {

	public CalendarAdapterListener calendarListener;
	private Context context;
	private Month month = new Month();

	private int thisYear;
	private int thisMonth;
	private int thisDate;

	public CalendarAdapter(Context context) {
		this.context = context;
		thisYear = Calendar.getInstance().get(Calendar.YEAR);
		thisMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
		thisDate = Calendar.getInstance().get(Calendar.DATE);
	}

	@Override
	public int getCount() {
		return this.month.getLastDate() + this.month.getDayOfWeek() - 1;
	}

	@Override
	public Object getItem(int i) {
		return null;
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.item_calendar_cell, viewGroup, false);

		} else {

		}

		TextView dateText = view.findViewById(R.id.item_calendar_date);
		ImageView firstIndicator = view.findViewById(R.id.item_calendar_first_indicator);
		ImageView secondIndicator = view.findViewById(R.id.item_calendar_second_indicator);
		ImageView thirdIndicator = view.findViewById(R.id.item_calendar_third_indicator);

		dateText.setText("");
		firstIndicator.setVisibility(View.GONE);
		secondIndicator.setVisibility(View.GONE);
		thirdIndicator.setVisibility(View.GONE);

		if (position + 1 < month.getDayOfWeek()) {
			view.setVisibility(View.INVISIBLE);
			view.setOnClickListener(null);
			return view;
		} else {
			view.setVisibility(View.VISIBLE);
			int date = position + 1 - month.getDayOfWeek() + 1;
			dateText.setText(String.valueOf(date));

			fillToday(view, dateText, month.getYear(), month.getMonth(), date);
			fillCell(date, firstIndicator, secondIndicator, thirdIndicator);

			view.setOnClickListener((View v) -> {
				calendarListener.showInputDialog(month.getYear(), month.getMonth(), date);
			});

			return view;
		}
	}

	private void setIndicatorColor(ImageView indicator, String color) {
		GradientDrawable shape = (GradientDrawable) indicator.getDrawable();
		switch (color) {
			case "tomato":
				shape.setColor(context.getColor(R.color.tomato));
				break;
			case "coral":
				shape.setColor(context.getColor(R.color.coral));
				break;
			case "salmon":
				shape.setColor(context.getColor(R.color.salmon));
				break;
			case "orange":
				shape.setColor(context.getColor(R.color.orange));
				break;
			case "light_green":
				shape.setColor(context.getColor(R.color.light_green));
				break;
			case "corn_flower_blue":
				shape.setColor(context.getColor(R.color.corn_flower_blue));
				break;
			case "deep_sky_blue":
				shape.setColor(context.getColor(R.color.deep_sky_blue));
				break;
			case "dodger_blue":
				shape.setColor(context.getColor(R.color.dodger_blue));
				break;
			case "medium_blue":
				shape.setColor(context.getColor(R.color.medium_blue));
				break;
			case "navy":
				shape.setColor(context.getColor(R.color.navy));
				break;
//			case "orange_":
//				shape.setColor(context.getColor(R.color.orange_));
//				break;
//			case "yellow_":
//				shape.setColor(context.getColor(R.color.yellow_));
//				break;
//			case "turquoise_":
//				shape.setColor(context.getColor(R.color.turquoise_));
//				break;
//			case "blueblack_":
//				shape.setColor(context.getColor(R.color.blueblack_));
//				break;
			case "color0":
				shape.setColor(context.getColor(R.color.color0));
				break;
			case "color1":
				shape.setColor(context.getColor(R.color.color1));
				break;
			case "color2":
				shape.setColor(context.getColor(R.color.color2));
				break;
			case "color3":
				shape.setColor(context.getColor(R.color.color3));
				break;
			case "color4":
				shape.setColor(context.getColor(R.color.color4));
				break;
			case "color5":
				shape.setColor(context.getColor(R.color.color5));
				break;
			case "color6":
				shape.setColor(context.getColor(R.color.color6));
				break;
			case "color7":
				shape.setColor(context.getColor(R.color.color7));
				break;
			default:
				break;
		}
	}

	private void fillToday(View view, TextView textView, int year, int month, int date){
		if(year == thisYear && month == thisMonth && date == thisDate){
//			view.setBackgroundColor(view.getResources().getColor(R.color.yellow, context.getTheme()));
//					.setBackgroundColor(view.getResources().getColor(R.color.yellow, context.getTheme()));
			view.findViewById(R.id.item_calendar_today).setVisibility(View.VISIBLE);
			textView.setTextColor(view.getResources().getColor(R.color.white, context.getTheme()));
		}
		else {
			view.findViewById(R.id.item_calendar_today).setVisibility(View.GONE);
			textView.setTextColor(view.getResources().getColor(R.color.gray, context.getTheme()));
		}
	}

	private void fillCell(int date, ImageView firstIndicator, ImageView secondIndicator, ImageView thirdIndicator) {
		boolean firstChecker = false;
		boolean secondChecker = false;
		boolean thirdChecker = false;
		for (int jobCount = 0; jobCount < month.getJobList().size(); jobCount++) {
			Job _job = month.getJobList().get(jobCount);
			for (int dailyCount = 0; dailyCount < _job.getDailyPayList().size(); dailyCount++) {
				DailyPay _dailyPay = _job.getDailyPayList().get(dailyCount);
				if (_dailyPay.date == date) {
					if (!firstChecker) {
						firstIndicator.setVisibility(View.VISIBLE);
						setIndicatorColor(firstIndicator, _job.jobColor);
						firstChecker = true;
					} else if (!secondChecker) {
						secondIndicator.setVisibility(View.VISIBLE);
						setIndicatorColor(secondIndicator, _job.jobColor);
						secondChecker = true;
					} else if (!thirdChecker) {
						thirdIndicator.setVisibility(View.VISIBLE);
						setIndicatorColor(thirdIndicator, _job.jobColor);
						thirdChecker = true;
					}
				}
			}
		}
	}

	public void setData(Month month) {
		this.month = month;
	}

	public interface CalendarAdapterListener {
		void showInputDialog(int year, int month, int date);
	}
}
