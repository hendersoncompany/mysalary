package com.henderson.mysalary.model;

public class SalaryInfo {
	public String jobName;
	public int salary;
	public String jobColor;
	public int thisYear;
	public int thisMonth;
	public int prevYear;
	public int prevMonth;
	public int payday;
}
