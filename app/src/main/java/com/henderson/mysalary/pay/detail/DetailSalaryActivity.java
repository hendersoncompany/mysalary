package com.henderson.mysalary.pay.detail;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.henderson.mysalary.BaseActivity;
import com.henderson.mysalary.R;
import com.henderson.mysalary.adapter.SalaryWorkListAdapter;
import com.henderson.mysalary.databinding.ActivityDetailSalaryBinding;
import com.henderson.mysalary.dialog.UpdatePayDialog;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.utils.Constant;

public class DetailSalaryActivity extends BaseActivity implements DetailSalaryListener, SalaryWorkListAdapter.SalaryWorkListListener, UpdatePayDialog.InputPayDialogListener {

	private DetailSalaryViewModel viewModel;
	private ActivityDetailSalaryBinding binding;

	private SalaryWorkListAdapter salaryWorkListAdapter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		initToolBar();
		initAd();

		viewModel.onCreate(this);
		addObserver();
	}

	private void initView(){
		binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_salary);
		viewModel = ViewModelProviders.of(this).get(DetailSalaryViewModel.class);
		viewModel.detailSalaryListener= this;
		binding.setViewmodel(viewModel);
		binding.setLifecycleOwner(this);

//		binding.detailSalaryMemoData.setOnTouchListener(new View.OnTouchListener() {
//			@Override
//			public boolean onTouch(View v, MotionEvent event) {
//				if(v.hasFocus()){
//					v.getParent().requestDisallowInterceptTouchEvent(true);
//					switch (event.getAction() & MotionEvent.ACTION_MASK){
//						case MotionEvent.ACTION_SCROLL:
//							v.getParent().requestDisallowInterceptTouchEvent(false);
//							return true;
//					}
//				}
//				return false;
//			}
//		});

		viewModel.salaryType = (Constant.SalaryType)getIntent().getSerializableExtra(Constant.SALARYTYPE);

		//Set SalaryWorkListAdapter
		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
		binding.detailSalaryRecyclerview.setLayoutManager(layoutManager);
		salaryWorkListAdapter = new SalaryWorkListAdapter(this, viewModel.salaryType);
		salaryWorkListAdapter.salaryWorkListListener = this;
		binding.detailSalaryRecyclerview.setAdapter(salaryWorkListAdapter);

		//Set View
		switch (viewModel.salaryType) {
			case DAILY_PAY:
				binding.detailSalaryPay.setText(R.string.dailypay);
				binding.detailSalaryWorkingTime.setVisibility(View.GONE);
				binding.detailSalaryWeeklyBonus.setVisibility(View.GONE);
				break;
			case HOURLY_PAY:
				binding.detailSalaryPay.setText(R.string.hourlypay);
				binding.detailSalaryWorkingTime.setVisibility(View.VISIBLE);
				binding.detailSalaryWeeklyBonus.setVisibility(View.VISIBLE);
				break;
		}
	}

	private void initToolBar(){
		setSupportActionBar(binding.detailSalaryToolbar);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	private void initAd(){
		MobileAds.initialize(this, new OnInitializationCompleteListener() {
			@Override
			public void onInitializationComplete(InitializationStatus initializationStatus) {
			}
		});
		AdRequest adRequest = new AdRequest.Builder().build();
		binding.detailSalaryAd.loadAd(adRequest);
	}

	private void addObserver(){
		//Set Observer
		viewModel.weeklyBonusSwitch.observe(this, value -> {
			viewModel.initData();
		});
		viewModel.insuranceSwitch.observe(this, value -> {
			viewModel.calculateSalaryInfo();
		});
	}

	//DetailSalaryListener Interface
	@Override
	public void finishInitData() {
		salaryWorkListAdapter.setWorkList(viewModel.payList);
		salaryWorkListAdapter.notifyDataSetChanged();
	}

	@Override
	public void clickWeeklyBonusDetail() {
		if(binding.detailSalaryWeeklyBonusDetail.getVisibility() == View.VISIBLE){
			binding.detailSalaryWeeklyBonusDetail.setVisibility(View.GONE);
			binding.detailSalaryWeeklyBonusButton.setBackgroundResource(R.drawable.ic_expand);
		}
		else {
			binding.detailSalaryWeeklyBonusDetail.setVisibility(View.VISIBLE);
			binding.detailSalaryWeeklyBonusButton.setBackgroundResource(R.drawable.ic_fold);
		}
	}

	//SalaryWorkListListener Interface
	private UpdatePayDialog dialog;
	@Override
	public void clickEdit(DailyPay dailypay) {
		if(viewModel.salaryType == Constant.SalaryType.HOURLY_PAY)
			dialog = new UpdatePayDialog(this, dailypay, UpdatePayDialog.Type.HOURLY);
		else
			dialog = new UpdatePayDialog(this, dailypay, UpdatePayDialog.Type.DAILY);
		dialog.listener = this;
		dialog.show();
	}

	@Override
	public void clickDelete(int id) {
		AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
		deleteDialog.setTitle(getResources().getString(R.string.dialog_delete_msg));
		deleteDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				viewModel.deleteDailyPay(id);
			}
		});
		deleteDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		deleteDialog.show();
	}

	//InputPayDialogListener Interface
	@Override
	public void closeDialog() {
		viewModel.updateDailyPay();
	}
}
