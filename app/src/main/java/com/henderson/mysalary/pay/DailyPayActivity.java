package com.henderson.mysalary.pay;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.henderson.mysalary.BaseActivity;
import com.henderson.mysalary.R;
import com.henderson.mysalary.adapter.SalaryInfoAdapter;
import com.henderson.mysalary.dialog.InputPayDialog;
import com.henderson.mysalary.databinding.ActivityDailypayBinding;
import com.henderson.mysalary.adapter.CalendarAdapter;
import com.henderson.mysalary.model.Month;
import com.henderson.mysalary.model.SalaryInfo;
import com.henderson.mysalary.pay.detail.DetailSalaryActivity;
import com.henderson.mysalary.utils.Constant;

import java.util.ArrayList;

import static com.henderson.mysalary.utils.BaseUtil.toast;

public class DailyPayActivity extends BaseActivity implements PayListener, CalendarAdapter.CalendarAdapterListener, InputPayDialog.InputPayDialogListener, SalaryInfoAdapter.SalaryInfoListener {

	private DailyPayViewModel viewModel;
	private ActivityDailypayBinding binding;

	private CalendarAdapter calendarAdapter;
	private SalaryInfoAdapter salaryInfoAdapter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		initToolBar();
		initAd();

		viewModel.onCreate(this);
	}

	private void initView(){
		//Set Databinding
		binding = DataBindingUtil.setContentView(this, R.layout.activity_dailypay);
		viewModel = ViewModelProviders.of(this).get(DailyPayViewModel.class);
		viewModel.payListener = this;
		binding.setViewmodel(viewModel);
		binding.setLifecycleOwner(this);

		//Set Calendar Adapter
		calendarAdapter = new CalendarAdapter(this);
		calendarAdapter.calendarListener = this;
		binding.dailypayCalendar.setAdapter(calendarAdapter);

		//Set JobInfo Adapter
		LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//		binding.dailypayJobInfo.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
		binding.dailypayJobInfo.setLayoutManager(layoutManager);
		salaryInfoAdapter = new SalaryInfoAdapter(this);
		salaryInfoAdapter.listener = this;
		binding.dailypayJobInfo.setAdapter(salaryInfoAdapter);
	}

	private void initToolBar(){
		setSupportActionBar(binding.dailypayToolbar);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	private void initAd(){
		MobileAds.initialize(this, new OnInitializationCompleteListener() {
			@Override
			public void onInitializationComplete(InitializationStatus initializationStatus) {
			}
		});
		AdRequest adRequest = new AdRequest.Builder().build();
		binding.dailypayAd.loadAd(adRequest);
	}

	@Override
	public void setCalendar(Month month) {
		calendarAdapter.setData(month);
		calendarAdapter.notifyDataSetChanged();
	}

	@Override
	public void setPaydayInfo(ArrayList<SalaryInfo> paydayList){
		if(paydayList.size() != 0) {
			binding.dailypaySalaryInfo.setText(R.string.salary_list_info);
			salaryInfoAdapter.setData(paydayList);
			salaryInfoAdapter.notifyDataSetChanged();
		}
		else {
			binding.dailypaySalaryInfo.setText(R.string.salary_empty_info);
			salaryInfoAdapter.setData(paydayList);
			salaryInfoAdapter.notifyDataSetChanged();
		}
	}


	private InputPayDialog dialog;
	@Override
	public void showInputDialog(int year, int month, int date) {
		if(viewModel.checkJob()) {
			toast(this, R.string.toast_empty_job);
		}
		else {
			dialog = new InputPayDialog(this, InputPayDialog.Type.DAILY, year, month, date);
			dialog.listener = this;
			dialog.show();
		}
	}

	@Override
	public void clickSalary(String jobName) {
		Intent intent = new Intent(this, DetailSalaryActivity.class);

		intent.putExtra(Constant.SALARYTYPE, Constant.SalaryType.DAILY_PAY);
		intent.putExtra(Constant.JOBNAME, jobName);
		intent.putExtra(Constant.THISYEAR, viewModel.thisMonth.getYear());
		intent.putExtra(Constant.THISMONTH, viewModel.thisMonth.getMonth());
		intent.putExtra(Constant.THISMONTH_FIRST_DAY, viewModel.thisMonth.getDayOfWeek());
		intent.putExtra(Constant.PREVYEAR, viewModel.prevMonth.getYear());
		intent.putExtra(Constant.PREVMONTH, viewModel.prevMonth.getMonth());
		intent.putExtra(Constant.PREVMONTH_FIRST_DAY, viewModel.prevMonth.getDayOfWeek());

		startActivity(intent);
	}

	@Override
	public void closeDialog() {
//		viewModel.addDailyPay(dailyPay);
		viewModel.setJobData();
		viewModel.setSalaryInfo();
	}
}
