package com.henderson.mysalary.pay;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.henderson.mysalary.BaseActivity;
import com.henderson.mysalary.R;
import com.henderson.mysalary.databinding.ActivityExpectedpayBinding;

public class ExpectedPayActivity extends BaseActivity {

	private ExpectedPayViewModel viewModel;
	private ActivityExpectedpayBinding binding;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		initToolBar();
		initAd();
	}

	@Override
	protected void onResume() {
		super.onResume();

		viewModel.onResume(this);
	}

	private void initView(){
		//Set Databinding
		binding = DataBindingUtil.setContentView(this, R.layout.activity_expectedpay);
		viewModel = ViewModelProviders.of(this).get(ExpectedPayViewModel.class);
		binding.setViewmodel(viewModel);
		binding.setLifecycleOwner(this);

		//Set Observer
		viewModel.payment.observe(this, value -> {
			viewModel.calculate();
		});
		viewModel.workingTime.observe(this, value -> {
			viewModel.calculate();
		});
		viewModel.workingDay.observe(this, value -> {
			viewModel.calculate();
		});
		viewModel.insuranceSwitch.observe(this, value -> {
			viewModel.calculate();
		});
		viewModel.weeklyAllowanceSwitch.observe(this, value -> {
			viewModel.calculate();
		});
	}

	private void initToolBar(){
		setSupportActionBar(binding.expectedpayToolbar);

		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	private void initAd(){
		MobileAds.initialize(this, new OnInitializationCompleteListener() {
			@Override
			public void onInitializationComplete(InitializationStatus initializationStatus) {
			}
		});
		AdRequest adRequest = new AdRequest.Builder().build();
		binding.expectedpayAd.loadAd(adRequest);
	}
}
