package com.henderson.mysalary.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.henderson.mysalary.R;
import com.henderson.mysalary.model.DailyPay;
import com.henderson.mysalary.model.Job;
import com.henderson.mysalary.utils.Constant;
import com.henderson.mysalary.utils.StringUtil;
import com.henderson.mysalary.utils.db.AppDatabase;

import java.util.ArrayList;
import java.util.List;

import static com.henderson.mysalary.utils.BaseUtil.toast;

//This dialog is implemented MVC, not MVVM
public class InputPayDialog extends Dialog {
	private List<DailyPay> addedWorkingList = new ArrayList<>();

	public enum Type {
		HOURLY, DAILY
	}

	private Context context;
	public InputPayDialogListener listener;
	private JobNameListAdapter adapter;

	//Resource variable
	private RadioGroup radioGroup;
	private RadioButton addJobButton;
	private RadioButton updateJobButton;
	private TextView dialogTitle;
	private TextView payment;
	private EditText paymentData;
	private TextView workingTime;
	private EditText hourData;
	private TextView hourText;
	private EditText minuteData;
	private TextView minuteText;
	private Button positiveButton;
	private Button deleteButton;

	//Resource Data
	private int year, month, date;
	private Type type;

	public InputPayDialog(@NonNull Context context, Type type, int year, int month, int date) {
		super(context);
		this.context = context;
		this.type = type;
		this.year = year;
		this.month = month;
		this.date = date;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initView();
		setRecyclerView();
		setView();
	}

	private void initView() {
		setContentView(R.layout.dialog_input_pay);

		Window window = this.getWindow();
		window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		//Set resource variable
		radioGroup = findViewById(R.id.input_pay_radio);
		addJobButton = findViewById(R.id.input_pay_radio_add);
		updateJobButton = findViewById(R.id.input_pay_radio_update);
		dialogTitle = findViewById(R.id.input_pay_title);
		payment = findViewById(R.id.input_pay_payment);
		paymentData = findViewById(R.id.input_pay_payment_data);
		workingTime = findViewById(R.id.input_pay_working_time);
		hourData = findViewById(R.id.input_pay_hour_data);
		hourText = findViewById(R.id.input_pay_hour);
		minuteData = findViewById(R.id.input_pay_minute_data);
		minuteText = findViewById(R.id.input_pay_minute);
		positiveButton = findViewById(R.id.input_pay_positive_button);
		deleteButton = findViewById(R.id.input_pay_delete_button);

		//Set Text
		switch (type) {
			case HOURLY:
				dialogTitle.setText(R.string.dialog_input_pay_dailypay_title_job_list);
				payment.setText(R.string.dialog_input_pay_hourlypay);
				workingTime.setVisibility(View.VISIBLE);
				hourData.setVisibility(View.VISIBLE);
				hourText.setVisibility(View.VISIBLE);
				minuteData.setVisibility(View.VISIBLE);
				minuteText.setVisibility(View.VISIBLE);
				break;
			case DAILY:
				dialogTitle.setText(R.string.dialog_input_pay_dailypay_title_job_list);
				payment.setText(R.string.dialog_input_pay_dailypay);
				workingTime.setVisibility(View.GONE);
				hourData.setVisibility(View.GONE);
				hourText.setVisibility(View.GONE);
				minuteData.setVisibility(View.GONE);
				minuteText.setVisibility(View.GONE);
				break;
		}

		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
		RecyclerView recyclerView = findViewById(R.id.input_pay_job_list);
		recyclerView.setLayoutManager(layoutManager);

		adapter = new JobNameListAdapter();
		recyclerView.setAdapter(adapter);

		//Set Event to Button
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				setRecyclerView();
				setView();
			}
		});

		findViewById(R.id.input_pay_negative_button).setOnClickListener((View view) -> {
			dismiss();
		});

		positiveButton.setOnClickListener((View view) -> {
			if(checkInputData()) {
				if (addJobButton.isChecked()) {
					addData();
				} else {
					updateData();
				}
			}
		});

		deleteButton.setOnClickListener((View view) -> {
			deleteData();
		});
	}

	private void setRecyclerView() {
		switch (type) {
			case HOURLY:
				addedWorkingList = AppDatabase.getAppDatabase(context).getWorkingList(0, year, month, date);
				break;
			case DAILY:
				addedWorkingList = AppDatabase.getAppDatabase(context).getWorkingList(1, year, month, date);
				break;
		}

		if (addedWorkingList.isEmpty()) updateJobButton.setEnabled(false);
		else updateJobButton.setEnabled(true);

		if (addJobButton.isChecked()) {
			setJobNameList();
		} else {
			setWorkingList();
		}
	}

	private void setView() {
		if (addJobButton.isChecked()) {
			positiveButton.setText(R.string.ok);
			positiveButton.setTextColor(context.getColor(R.color.dodger_blue));
			deleteButton.setVisibility(View.GONE);
			setAddView();
		} else {
			positiveButton.setText(R.string.update);
			positiveButton.setTextColor(context.getColor(R.color.line_green));
			deleteButton.setVisibility(View.VISIBLE);
			setUpdateView();
		}
	}

	private void setAddView() {
		Job _job = adapter.getJob();
		DailyPay _pay;

		paymentData.setText("" + _job.payment);

		switch (type) {
			case HOURLY:
				_pay = AppDatabase.getAppDatabase(context).getLastDailyPay(0, _job.jobName);
				paymentData.setHint(R.string.dialog_input_pay_hourlypay_hint);
				if (_pay == null || _pay.hourlyPayment == 0) {
					hourData.setText("");
					hourData.setHint("8");
					minuteData.setText("");
					minuteData.setHint("0");
				}
				else {
					hourData.setText("" + _pay.workingTime / 60);
					minuteData.setText("" + (_pay.workingTime - ((_pay.workingTime / 60) * 60)));
				}
				break;
			case DAILY:
				paymentData.setHint(R.string.dialog_input_pay_dailypay_hint);
				break;
		}
	}

	private void setUpdateView() {
		String selectedJob = adapter.getJob().jobName;
		for (int i = 0; i < adapter.jobList.size(); i++) {
			if (addedWorkingList.get(i).jobName.equals(selectedJob)) {
				DailyPay _pay = addedWorkingList.get(i);
				switch (type) {
					case HOURLY:
						paymentData.setText("" + _pay.hourlyPayment);
						hourData.setText("" + _pay.workingTime / 60);
						minuteData.setText("" + (_pay.workingTime - ((_pay.workingTime / 60) * 60)));
						break;
					case DAILY:
						paymentData.setText("" + _pay.dailyPayment);
						break;
				}
				break;
			}
		}
	}

	private boolean checkInputData() {
		if (!StringUtil.checkStringWithZero(paymentData)) {
			switch (type) {
				case HOURLY:
					toast(context, R.string.toast_empty_hourly_pay);
					break;
				case DAILY:
					toast(context, R.string.toast_empty_daily_pay);
					break;
			}
			return false;
		} else if (this.type == Type.HOURLY  && !StringUtil.checkStringWithZero(hourData)) {
			toast(context, R.string.toast_empty_working_time);
			return false;
		}
		return true;
	}

	private void addData(){
		AppDatabase.getAppDatabase(context).insertDailyPay(setDailyPay(calculateWorkingTime()));
		toast(context, R.string.toast_success_add_job);
		listener.closeDialog();
		dismiss();
	}

	private void updateData(){
		DailyPay _dailyPay = setDailyPay(calculateWorkingTime());
		for(int i=0; i<addedWorkingList.size(); i++){
			if(addedWorkingList.get(i).jobName.equals(_dailyPay.jobName)){
				_dailyPay.id = addedWorkingList.get(i).id;
			}
		}

		AppDatabase.getAppDatabase(context).updateDailyPay(_dailyPay);
		toast(context, R.string.toast_success_update);
		listener.closeDialog();
		dismiss();
	}

	private void deleteData(){
		String _jobName = adapter.getJob().jobName;
		for(int i=0; i<addedWorkingList.size(); i++){
			if(addedWorkingList.get(i).jobName.equals(_jobName)){
				AppDatabase.getAppDatabase(context).deleteDailyPay(addedWorkingList.get(i).id);
				toast(context, R.string.toast_success_delete);
				listener.closeDialog();
				dismiss();
				return;
			}
		}
	}

	private int calculateWorkingTime() {
		if (!StringUtil.checkString(hourData)) {
			return 0;
		}
		int _hour = Integer.parseInt(hourData.getText().toString());
		int _minute = StringUtil.checkString(minuteData) ? Integer.parseInt(minuteData.getText().toString()) : 0;

		return (_hour * 60) + _minute;
	}

	private DailyPay setDailyPay(int workingTime) {
		DailyPay _dailyPay = new DailyPay();
		_dailyPay.year = this.year;
		_dailyPay.month = this.month;
		_dailyPay.date = this.date;

		switch (type) {
			case HOURLY:
				_dailyPay.salaryType = 0;
				_dailyPay.jobName = adapter.getJob().jobName;
				_dailyPay.hourlyPayment = Integer.parseInt(paymentData.getText().toString());
				_dailyPay.workingTime = workingTime;
				break;
			case DAILY:
				_dailyPay.salaryType = 1;
				_dailyPay.jobName = adapter.getJob().jobName;
				_dailyPay.dailyPayment = Integer.parseInt(paymentData.getText().toString());
				break;
		}
		return _dailyPay;
	}

	/**
	 * 알바 등록할 때 필요한 알바 리스트를 가져와서 Set RecyclerView
	 */
	private void setJobNameList() {
		List<Job> jobList = AppDatabase.getAppDatabase(getContext()).getJobList();
		adapter.setData(jobList);
		adapter.notifyDataSetChanged();
	}

	/**
	 * 알바 수정할 때 필요한 근무 내역 리스트를 가져와서 Set RecyclerView
	 */
	private void setWorkingList() {
		List<Job> jobList = new ArrayList<>();
		for (int i = 0; i < addedWorkingList.size(); i++) {
			jobList.add(AppDatabase.getAppDatabase(context).getJob(addedWorkingList.get(i).jobName));
		}

		adapter.setData(jobList);
		adapter.notifyDataSetChanged();
	}

	private class JobNameListAdapter extends RecyclerView.Adapter<JobNameListAdapter.ViewHolder> {
		private List<Job> jobList = new ArrayList<>();
		private int selectedItem = 0;

		public class ViewHolder extends RecyclerView.ViewHolder {
			ImageView indicator;
			TextView jobName;
			ImageView checker;

			ViewHolder(View itemView) {
				super(itemView);
				indicator = itemView.findViewById(R.id.job_name_indicator);
				jobName = itemView.findViewById(R.id.job_name_name);
				checker = itemView.findViewById(R.id.job_indicator_checker);

				itemView.setOnClickListener((View view) -> {
					selectedItem = getAdapterPosition();
					notifyDataSetChanged();
					setView();
				});
			}
		}

		public Job getJob() {
			return jobList.get(selectedItem);
		}

		@NonNull
		@Override
		public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			View view = inflater.inflate(R.layout.item_job_name, parent, false);
			JobNameListAdapter.ViewHolder viewHolder = new JobNameListAdapter.ViewHolder(view);

			return viewHolder;
		}

		@Override
		public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
			GradientDrawable shape = (GradientDrawable) holder.indicator.getDrawable();
			switch (jobList.get(position).jobColor) {
				case "tomato":
					shape.setColor(getContext().getColor(R.color.tomato));
					break;
				case "coral":
					shape.setColor(getContext().getColor(R.color.coral));
					break;
				case "salmon":
					shape.setColor(getContext().getColor(R.color.salmon));
					break;
				case "orange":
					shape.setColor(getContext().getColor(R.color.orange));
					break;
				case "light_green":
					shape.setColor(getContext().getColor(R.color.light_green));
					break;
				case "corn_flower_blue":
					shape.setColor(getContext().getColor(R.color.corn_flower_blue));
					break;
				case "deep_sky_blue":
					shape.setColor(getContext().getColor(R.color.deep_sky_blue));
					break;
				case "dodger_blue":
					shape.setColor(getContext().getColor(R.color.dodger_blue));
					break;
				case "medium_blue":
					shape.setColor(getContext().getColor(R.color.medium_blue));
					break;
				case "navy":
					shape.setColor(getContext().getColor(R.color.navy));
					break;
//				case "orange_":
//					shape.setColor(getContext().getColor(R.color.orange_));
//					break;
//				case "yellow_":
//					shape.setColor(getContext().getColor(R.color.yellow_));
//					break;
//				case "turquoise_":
//					shape.setColor(getContext().getColor(R.color.turquoise_));
//					break;
//				case "blueblack_":
//					shape.setColor(getContext().getColor(R.color.blueblack_));
//					break;
				case "color0":
					shape.setColor(getContext().getColor(R.color.color0));
					break;
				case "color1":
					shape.setColor(getContext().getColor(R.color.color1));
					break;
				case "color2":
					shape.setColor(getContext().getColor(R.color.color2));
					break;
				case "color3":
					shape.setColor(getContext().getColor(R.color.color3));
					break;
				case "color4":
					shape.setColor(getContext().getColor(R.color.color4));
					break;
				case "color5":
					shape.setColor(getContext().getColor(R.color.color5));
					break;
				case "color6":
					shape.setColor(getContext().getColor(R.color.color6));
					break;
				case "color7":
					shape.setColor(getContext().getColor(R.color.color7));
					break;
				default:
					break;
			}
			holder.jobName.setText(jobList.get(position).jobName);

			if (position == selectedItem) {
				holder.checker.setBackgroundResource(R.drawable.ic_check);
			} else {
				holder.checker.setBackgroundResource(0);
			}
		}

		@Override
		public int getItemCount() {
			return jobList.size();
		}

		public void setData(List<Job> jobList) {
			this.jobList = jobList;
			this.selectedItem = 0;
		}
	}

	public interface InputPayDialogListener {
		void closeDialog();
	}
}
